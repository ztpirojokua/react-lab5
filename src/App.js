import "./App.css";

import FeedbackForm from "./components/FeedbackForm/FeedbackForm";
import NewPostForm from "./components/NewPost/NewPostForm";
import { Container } from "@mui/material";

function App () {

  const cityOptions = [
    "Житомир",
    "Київ",
    "Вінниця",
    "Тернопіль",
    "Львів",
    "Одеса",
    "Херсон",
    "Запоріжжя",
    "Донецьк",
    "Харків",
    "Луганськ"
  ];

  const packingOptions = [
    "Конверт з ПБ плівкою С/13 (150х215) мм",
    "Конверт з ПБ плівкою D/14 (180х265) мм",
    "Конверт з ПБ плівкою E/15 (220х265) мм",
    "Коробка (0.5 кг) пласка",
    "Коробка (0.5 кг) стандартна",
    "Коробка (0.5 кг) з наповнювачем",
    "Коробка (10 кг) стандартна"
  ];

  const palletTypeOptions = [
    "Палета від 1,5 м2 до 2 м2 (814)",
    "Палета від 1,5 м2 до 2 м2 (815)",
    "Палета від 1,5 м2 до 2 м2 (816)",
    "Палета від 1 м2 до 1,49 м2 (612)"
  ];

  const deliveryTypeOptions = ["Посилки", "Палети"];

  const returnTypeOptions = ["Документи", "Грошовий переказ"];

  return (
    <Container>
      <FeedbackForm />
      <NewPostForm
        cityOptions={cityOptions}
        returnTypeOptions={returnTypeOptions}
        packingOptions={packingOptions}
        palletTypeOptions={palletTypeOptions}
        deliveryTypeOptions={deliveryTypeOptions}
      />
    </Container>
  );

}

export default App;
