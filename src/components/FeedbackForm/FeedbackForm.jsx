import { Box, Button, TextField } from "@mui/material";
import { useState } from "react";

const FeedbackForm = () => {

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [topic, setTopic] = useState("");
  const [message, setMessage] = useState("");

  const handleFormSubmit = (event) => {
    event.preventDefault();

    console.log("form successfully sent");

    setName("");
    setEmail("");
    setTopic("");
    setMessage("");
  };

  const validateTopic = () => {
    return topic.length > 1 || topic === "";
  };

  const validateEmail = () => {
    let regex = /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/;

    return regex.test(email) || email === "";
  };

  return (
    <Box style={{ border: "2px solid purple", padding: "20px", margin: "10px" }}>
      <h2>Feedback form</h2>
      <form onSubmit={handleFormSubmit}>
        <Box sx={{ display: "flex", flexDirection: "column", alignItems: "left" }}>
          <TextField
            label={"Name"}
            type={"text"}
            value={name}
            onChange={(event) => setName(event.target.value)}
          />
          <TextField
            label={"Email"}
            type={"email"}
            required
            error={!validateEmail()}
            helperText={!validateEmail() ? "Invalid email" : ""}
            value={email}
            onChange={(event) => setEmail(event.target.value)}
          />
          <TextField
            label={"Topic"}
            required
            error={!validateTopic()}
            helperText={!validateTopic() ? "The message must be longer than 1 character" : ""}
            type={"text"}
            value={topic}
            onChange={(event) => setTopic(event.target.value)}
          />
          <TextField
            label={"Message"}
            type={"textarea"}
            value={message}
            onChange={(event) => setMessage(event.target.value)}
          />
          <Button
            variant="contained"
            type={"submit"}
          >
            Send
          </Button>
        </Box>
      </form>
    </Box>
  );
};

export default FeedbackForm;