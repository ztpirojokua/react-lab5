import React from "react";

import { Controller } from "react-hook-form";
import { MenuItem, Select } from "@mui/material";

const CitySelect = ({ control, options, errors, name, typeName }) => {
  return (
    <>
      <p>{typeName}</p>
      <Controller
        name={name}
        id={name}
        defaultValue={options[0]}
        control={control}
        render={({ field }) => (
          <Select {...field} sx={{ width: "auto", margin: "5px" }}
                  error={errors?.[name]?.type === "required"}
          >
            {options.map(option => <MenuItem
              key={option}
              value={option}
            >{option}</MenuItem>)}
          </Select>
        )}
      />
      {errors?.[name]?.type === "required" && <p style={{ color: "red" }}>Вкажіть місто</p>}
    </>
  );
};

export default CitySelect;