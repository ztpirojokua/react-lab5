import React, { useEffect } from "react";
import * as yup from "yup";

import PalletSection from "./PalletSection";
import ParcelSection from "./ParcelSection";
import CitySelect from "./CitySelect";
import PackingOptions from "./PackingOptions";
import LiftSection from "./LiftSection";
import Return from "./Return";

import { Controller, useFieldArray, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Checkbox, FormControlLabel, Grid, MenuItem, Select } from "@mui/material";

const startData = {
  characteristic: [
    {
      palletType: "",
      placeCount: "",
      announcedPrice: "",
      weight: "",
      length: "",
      width: "",
      height: ""
    }]
};

const schema = yup.object().shape({
  recipient: yup.string().required(),
  departure: yup.string().required(),
  deliveryType: yup.string().required(),
  isElevator: yup.boolean(),
  returnType: yup.string().when("isReturn", {
    is: true,
    then: yup.string().required()
  }),
  floor: yup.number().positive().when("isElevator", {
    is: true,
    then: yup.number().required()
  }),
  characteristic: yup.array().of(
    yup.object().shape({
      palletType: yup.string(),
      announcedPrice: yup.number().positive(),
      placeCount: yup.number().integer().positive(),
      weight: yup.number().positive(),
      length: yup.number().positive(),
      width: yup.number().positive(),
      height: yup.number().positive()
    })
  )
});

const NewPostForm = ({
  deliveryTypeOptions,
  returnTypeOptions,
  packingOptions,
  cityOptions,
  palletTypeOptions
}) => {

  const { control, handleSubmit, watch, reset, formState: { errors } } = useForm({
    resolver: yupResolver(schema),
    shouldUnregister: true,
    defaultValues: {
      isPacking: false,
      isElevator: false,
      isReturn: false
    }
  });

  const { fields, append, remove } = useFieldArray({ name: "characteristic", control });

  const deliveryType = watch("deliveryType");
  const isPacking = watch("isPacking");
  const isReturn = watch("isReturn");

  useEffect(() => {
    reset(startData);
  }, []);

  const onSubmit = (data) => {
    console.log(data);
  };

  return (
    <div style={{ border: "2px solid purple", padding: "20px", margin: "10px" }}>
      <h2>Нова Пошта</h2>
      <form onSubmit={handleSubmit(onSubmit)}>
        <p>Маршрут</p>
        <Grid container>
          <Grid
            item
            xs={2}
          >
            <CitySelect
              control={control}
              options={cityOptions}
              errors={errors}
              name="departure"
              typeName="Місто відправник"
            />
          </Grid>
          <Grid
            item
            xs={10}
          >
            <CitySelect
              control={control}
              options={cityOptions}
              errors={errors}
              name="recipient"
              typeName="Місто одержувач"
            />
          </Grid>
        </Grid>
        <div>
          <h4>Вид відправлення</h4>
          <Controller
            name="deliveryType"
            id="deliveryType"
            defaultValue={deliveryTypeOptions[0]}
            control={control}
            render={({ field }) => (
              <Select {...field} error={errors.deliveryType?.type === "required"}>
                {deliveryTypeOptions.map(option =>
                  <MenuItem
                    key={option}
                    value={option}
                  >
                    {option}
                  </MenuItem>
                )}
              </Select>
            )}
          />
          {errors.deliveryType?.type === "required" && <p style={{ color: "red" }}>Вкажіть тип</p>}
        </div>
        <div>
          <p>Характеристика місць</p>
          {deliveryType === "Палети" ?
            <PalletSection
              control={control}
              fields={fields}
              remove={remove}
              append={append}
              errors={errors}
              options={palletTypeOptions}
            />
            :
            <ParcelSection
              control={control}
              fields={fields}
              remove={remove}
              append={append}
              errors={errors}
            />
          }
        </div>
        <Controller
          name="isPacking"
          control={control}
          render={({ field }) =>
            <FormControlLabel {...field} control={<Checkbox />}
                              label='Послуга "Пакування"'
            />}
        />
        {isPacking && <PackingOptions
          control={control}
          fields={fields}
          options={packingOptions}
        />}
        <LiftSection
          control={control}
          errors={errors}
        />
        <Grid container>
          <Grid xs={3}>
            <Controller
              name="isReturn"
              control={control}
              render={({ field }) => <FormControlLabel {...field} control={<Checkbox />}
                                                       label='Послуга "Зворотна доставка"'
              />}
            />
          </Grid>
        </Grid>
        {isReturn && <Return
          control={control}
          errors={errors}
          options={returnTypeOptions}
        />}
        <div>
          <Button
            variant="contained"
            color="success"
            type="submit"
          >
            Розрахувати вартість
          </Button>
          <Button
            onClick={reset}
            variant="contained"
            color="warning"
          >
            Очистити
          </Button>
        </div>
      </form>
    </div>
  );
};

export default NewPostForm;