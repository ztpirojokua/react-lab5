import React from "react";

import { Button, Grid, TextField } from "@mui/material";
import { Controller } from "react-hook-form";

const ParcelSection = ({ append, remove, control, fields, errors }) => {
  const addPlace = () => {
    append({
      placeCount: "",
      announcedPrice: "",
      weight: "",
      length: "",
      width: "",
      height: ""
    });
  };

  return (
    <>
      {fields.map((item, index) =>
        <Grid
          container
          key={index}
        >
          <Grid xs={1}>
            <Controller
              name={`characteristic[${index}].placeCount`}
              control={control}
              render={({ field }) =>
                <TextField
                  required
                  error={errors?.characteristic?.[index]?.placeCount}
                  {...field}
                  label="Кількість"
                  type="number"
                />}
            />
            {errors?.characteristic?.[index]?.placeCount?.type === "min" &&
              <p style={{ color: "red" }}>Число має бути додатнім</p>}
          </Grid>
          <Grid xs={2}>
            <Controller
              name={`characteristic[${index}].announcedPrice`}
              control={control}
              render={({ field }) =>
                <TextField
                  fullWidth
                  required
                  error={errors?.characteristic?.[index]?.announcedPrice}
                  {...field}
                  label="Оголошена вартість"
                />}
            />
            {errors?.characteristic?.[index]?.announcedPrice?.type === "typeError" &&
              <p style={{ color: "red" }}>Значення має бути числом</p>}
            {errors?.characteristic?.[index]?.announcedPrice?.type === "min" &&
              <p style={{ color: "red" }}>Число має бути додатнім</p>}
          </Grid>
          <Grid xs={1}>
            <Controller
              name={`characteristic[${index}].weight`}
              control={control}
              render={({ field }) =>
                <TextField
                  required
                  error={errors?.characteristic?.[index]?.weight}
                  {...field}
                  label="Вага"
                />}
            />
            {errors?.characteristic?.[index]?.weight?.type === "typeError" &&
              <p style={{ color: "red" }}>Значення має бути числом</p>}
            {errors?.characteristic?.[index]?.weight?.type === "min" &&
              <p style={{ color: "red" }}>Число має бути додатнім</p>}
          </Grid>
          <Grid xs={1}>
            <Controller
              name={`characteristic[${index}].length`}
              control={control}
              render={({ field }) =>
                <TextField
                  required
                  error={errors?.characteristic?.[index]?.length}
                  {...field}
                  label="Довжина"
                />}
            />
            {errors?.characteristic?.[index]?.length?.type === "typeError" &&
              <p style={{ color: "red" }}>Значення має бути числом</p>}
            {errors?.characteristic?.[index]?.length?.type === "min" &&
              <p style={{ color: "red" }}>Число має бути додатнім</p>}
          </Grid>
          <Grid xs={1}>
            <Controller
              name={`characteristic[${index}].width`}
              control={control}
              render={({ field }) =>
                <TextField
                  required
                  error={errors?.characteristic?.[index]?.width}
                  {...field}
                  label="Ширина"
                />}
            />
            {errors?.characteristic?.[index]?.width?.type === "typeError" &&
              <p style={{ color: "red" }}>Значення має бути числом</p>}
            {errors?.characteristic?.[index]?.width?.type === "min" &&
              <p style={{ color: "red" }}>Число має бути додатнім</p>}
          </Grid>
          <Grid xs={1}>
            <Controller
              name={`characteristic[${index}].height`}
              control={control}
              render={({ field }) =>
                <TextField
                  required
                  error={errors?.characteristic?.[index]?.height}
                  {...field}
                  label="Висота"
                />}
            />
            {errors?.characteristic?.[index]?.height?.type === "typeError" &&
              <p style={{ color: "red" }}>Значення має бути числом</p>}
            {errors?.characteristic?.[index]?.height?.type === "min" &&
              <p style={{ color: "red" }}>Число має бути додатнім</p>}
          </Grid>
          {index !== 0 && <Grid xs={3}>
            <Button onClick={() => remove(index)}>Видалити</Button>
          </Grid>}
        </Grid>)}
      <Button onClick={addPlace}>Додати місце</Button>
    </>
  );
};

export default ParcelSection;
